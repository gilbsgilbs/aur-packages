#!/usr/bin/env python3
import hashlib
import json
import os
import pathlib
import re
import shutil
import sys
import typing

import github
import jinja2
import requests

PKG_PATH = pathlib.Path("./pkg/")
DIST_PATH = pathlib.Path("./dist/")


class GithubReleaseParamsAssetConfig(typing.TypedDict):
    name_pattern: str


class GithubReleaseParams(typing.TypedDict):
    mode: typing.Literal["github-release"]
    repo_name: str
    tag_pattern: str
    tag_replace: typing.NotRequired[str]
    assets: typing.Dict[str, GithubReleaseParamsAssetConfig]
    prereleases_allowed: typing.NotRequired[bool]


class GithubTagParams(typing.TypedDict):
    mode: typing.Literal["github-tag"]
    repo_name: str
    tag_pattern: str
    tag_replace: typing.NotRequired[str]


class NoneParams(typing.TypedDict):
    mode: typing.Literal["none"]


MetadataParams = typing.Union[GithubReleaseParams, GithubTagParams, NoneParams]


class ReleaseInfo(typing.TypedDict):
    version: str
    tag_name: str
    commit_sha: str
    assets: typing.Dict[str, str]


class PackageData(typing.TypedDict):
    pkgver: str
    pkgrel: int


class Cache(typing.TypedDict):
    remote_sha512: typing.Dict[str, str]


class Metadata(typing.TypedDict):
    params: MetadataParams
    release_info: ReleaseInfo
    package_data: PackageData
    cache: Cache
    extra: typing.Dict[str, typing.Any]


def _get_version_from_tag(
    tag: str, pattern: str, replace: typing.Optional[str]
) -> typing.Optional[str]:
    if replace is None:
        replace = "{}"

    match = re.search(pattern, tag)
    if match is None:
        return None

    return replace.format(*(v for v in match.groups()), **match.groupdict())


def _new_github_client() -> github.Github:
    auth: typing.Optional[github.Auth] = None
    if token := os.getenv("GITHUB_TOKEN"):
        auth = github.Auth.Token(token)
    return github.Github(auth=auth)


def query_latest_release_info(old_metadata: Metadata) -> ReleaseInfo:
    params = old_metadata["params"]
    mode = params["mode"]
    if mode == "github-tag":
        params = typing.cast(GithubTagParams, params)
        repo_name = params["repo_name"]
        repo = _new_github_client().get_repo(repo_name)

        for tag in repo.get_tags():
            version = _get_version_from_tag(
                tag.name,
                params["tag_pattern"],
                params.get("tag_replace"),
            )
            if version is None:
                continue

            tarball_url = (
                f"https://github.com/{repo_name}/archive/refs/tags/{tag.name}.tar.gz"
            )
            return {
                "version": version,
                "tag_name": tag.name,
                "commit_sha": tag.commit.sha,
                "assets": {
                    "tarball_url": tarball_url,
                },
            }

        raise RuntimeError("No tag matching pattern found.")
    elif mode == "github-release":
        params = typing.cast(GithubReleaseParams, params)
        repo_name = params["repo_name"]
        repo = _new_github_client().get_repo(repo_name)

        for release in repo.get_releases():
            if release.prerelease and params.get("prereleases_allowed", False):
                continue

            version = _get_version_from_tag(
                release.tag_name,
                params["tag_pattern"],
                params.get("tag_replace"),
            )
            if version is None:
                continue

            assets: typing.Dict[str, str] = {}
            for asset_name, asset_config in params["assets"].items():
                for asset in release.get_assets():
                    asset_match = re.search(asset_config["name_pattern"], asset.name)
                    if asset_match is None:
                        continue

                    assets[asset_name] = asset.browser_download_url
                    break
                else:
                    raise RuntimeError(
                        "No asset matching pattern"
                        f" {asset_config['name_pattern']} found."
                    )

            tag = next(tag for tag in repo.get_tags() if tag.name == release.tag_name)
            return {
                "version": version,
                "tag_name": release.tag_name,
                "commit_sha": tag.commit.sha,
                "assets": assets,
            }

        raise RuntimeError("No tag matching pattern found.")
    elif mode == "none":
        return old_metadata["release_info"]
    raise ValueError(f"Unknown mode {mode}.")


def compute_remote_checksum(url: str, sumfunc=hashlib.sha512, chunk_size=1024) -> str:
    s = sumfunc()
    with requests.get(url, stream=True) as resp:
        for content in resp.iter_content(chunk_size=chunk_size):
            s.update(content)
    return s.hexdigest()


def refresh_metadata(old_metadata: Metadata, new_release_info: ReleaseInfo) -> Metadata:
    pkgrel = 1

    if new_release_info["commit_sha"] == old_metadata["release_info"]["commit_sha"]:
        # Existing release, get pkgrel from existing metadata.
        pkgrel = old_metadata["package_data"]["pkgrel"]
    elif new_release_info["tag_name"] == old_metadata["release_info"]["tag_name"]:
        # Retag?
        pkgrel = old_metadata["package_data"]["pkgrel"] + 1
        # Invalidate cache so that we always re-download assets.
        old_metadata["cache"]["remote_sha512"] = {}
    else:
        # New release, always reset pkgrel.
        pkgrel = 1

    new_metadata: Metadata = {
        "params": old_metadata["params"],
        "cache": old_metadata["cache"],
        "release_info": new_release_info,
        "package_data": {
            "pkgver": new_release_info["version"],
            "pkgrel": pkgrel,
        },
        "extra": old_metadata.get("extra", {}),
    }

    return new_metadata


def render(path: pathlib.Path, metadata: Metadata) -> Metadata:
    new_cache: Cache = {"remote_sha512": {}}

    dist_path = DIST_PATH / path.name

    # Remove any existing target directory.
    try:
        shutil.rmtree(str(dist_path))
    except FileNotFoundError:
        pass
    shutil.copytree(path, dist_path)

    def _sha512sum(target: str) -> str:
        if target.startswith(("http://", "https://")):
            try:
                checksum = metadata["cache"]["remote_sha512"][target]
            except KeyError:
                checksum = compute_remote_checksum(target)

            new_cache["remote_sha512"][target] = checksum
            return checksum

        path = dist_path / target
        return hashlib.sha512(path.read_bytes()).hexdigest()

    # Render templates
    env = jinja2.Environment(loader=jinja2.FileSystemLoader("."))
    env.globals["sha512sum"] = _sha512sum
    env.filters["regex_replace"] = lambda s, find, replace: re.sub(find, replace, s)
    for template_path in dist_path.glob("**/*.jinja"):
        template = env.get_template(str(template_path))
        result = template.render(metadata=metadata, pkgname=path.name)
        pathlib.Path(str(template_path).removesuffix(".jinja")).write_text(result)
        template_path.unlink()

    # Remove any file that starts with "_" (including _metadata.json).
    for path in dist_path.glob("_*"):
        if path.is_file():
            path.unlink()
        else:
            shutil.rmtree(str(path))

    return typing.cast(
        Metadata,
        {
            **metadata,
            "cache": new_cache,
        },
    )


def main(packages: typing.List[str]) -> bool:
    if not packages:
        packages = [path.name for path in PKG_PATH.glob("*") if path.is_dir()]

    for package in packages:
        package_path = PKG_PATH / package
        if not package_path.is_dir():
            raise RuntimeError(f"Unknown package {package}.")

        metadata_path = package_path / "_metadata.json"
        old_metadata: Metadata = json.loads(metadata_path.read_text())
        release_info = query_latest_release_info(old_metadata)
        new_metadata = refresh_metadata(old_metadata, release_info)
        new_metadata = render(package_path, new_metadata)
        metadata_path.write_text(
            json.dumps(new_metadata, sort_keys=True, indent=2) + "\n"
        )
    return True


if __name__ == "__main__":
    sys.exit(not bool(main(sys.argv[1:])))
