#!/usr/bin/env python3
import json
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile
import typing
import urllib.parse

PKG_DIR = pathlib.Path("pkg")
DIST_PATH = pathlib.Path("dist")


def this_repo_url() -> str:
    remote = (
        subprocess.check_output(["git", "remote", "get-url", "origin"]).decode().strip()
    )

    if remote.startswith("git@"):
        return remote

    parsed_url = urllib.parse.urlsplit(remote)

    username = parsed_url.username
    password = os.getenv("GITLAB_ACCESS_TOKEN") or parsed_url.password
    hostname = parsed_url.hostname
    port = parsed_url.port
    netloc = ""
    if password is not None or username is not None:
        netloc += "{}:{}@".format(
            urllib.parse.quote(username or ""),
            urllib.parse.quote(password or ""),
        )
    netloc += hostname or ""
    if port is not None:
        netloc += f":{port}"

    return parsed_url._replace(netloc=netloc).geturl()


def git_clone(
    repo_url: str,
    output_path: pathlib.Path,
    branch: typing.Optional[str] = None,
) -> None:
    branch_args = ["--branch", branch] if branch else []
    subprocess.check_call(["git", "clone", *branch_args, repo_url, str(output_path)])


def git_add(repo_path: pathlib.Path, path: pathlib.Path = pathlib.Path(".")):
    subprocess.check_call(["git", "add", str(path)], cwd=str(repo_path))


def git_commit(repo_path: pathlib.Path, commit_message: str):
    subprocess.check_call(["git", "commit", "-m", commit_message], cwd=str(repo_path))


def git_push(
    repo_path: pathlib.Path, dest: str, remote: str = "origin", ci_skip: bool = False
):
    ci_skip_args = ["-o", "ci.skip"] if ci_skip else []
    subprocess.check_call(
        ["git", "push", *ci_skip_args, remote, dest], cwd=str(repo_path)
    )


def git_remove_all_tracked(repo_path: pathlib.Path):
    subprocess.check_call(
        ["git", "rm", "--ignore-unmatch", "-r", "."], cwd=str(repo_path)
    )


def git_has_diff(repo_path: pathlib.Path) -> bool:
    try:
        subprocess.check_call(
            ["git", "diff", "--cached", "--exit-code"], cwd=str(repo_path)
        )
    except subprocess.CalledProcessError as exc:
        if exc.returncode == 1:
            return True
        raise
    return False


def main(packages: typing.List[str]) -> bool:
    if not packages:
        packages = [
            path.name for path in DIST_PATH.glob("*") if (path / "PKGBUILD").exists()
        ]

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmp_path = pathlib.Path(tmpdirname)

        # Need to clone this repo again because Gitlab-CI checkouts a specific
        # commit.
        this_repo_path = tmp_path / "aur-packages"
        git_clone(this_repo_url(), this_repo_path, branch=os.getenv("CI_COMMIT_BRANCH"))

        for package in packages:
            package_path = DIST_PATH / package
            if not package_path.is_dir():
                raise RuntimeError(f"Unknown package {package}.")

            if not (package_path / ".SRCINFO").exists():
                raise RuntimeError(f"Missing .SRCINFO for package {package}.")

            print(f"Processing package {package}.")
            aur_repo_path = tmp_path / package
            git_clone(f"ssh://aur@aur.archlinux.org/{package}.git", aur_repo_path)

            metadata_path = PKG_DIR / package / "_metadata.json"

            print("Parsing new metadata.")
            metadata = json.loads(metadata_path.read_text())
            pkgver = metadata["package_data"]["pkgver"]
            pkgrel = metadata["package_data"]["pkgrel"]
            has_committed = False

            print("Updating metadata.")
            shutil.copy(str(metadata_path), str(this_repo_path / metadata_path))
            git_add(this_repo_path, metadata_path)
            if git_has_diff(this_repo_path):
                has_committed = True
                git_commit(
                    this_repo_path, f"Update {package} metadata to {pkgver}-{pkgrel}."
                )

            print("Updating AUR package.")
            git_remove_all_tracked(aur_repo_path)
            for path in package_path.glob("*"):
                if path.is_dir():
                    shutil.copytree(str(path), str(aur_repo_path / path.name))
                else:
                    shutil.copy(str(path), str(aur_repo_path / path.name))
            git_add(aur_repo_path)
            if git_has_diff(aur_repo_path):
                has_committed = True
                git_commit(aur_repo_path, f"Update {package} to {pkgver}-{pkgrel}.")

            if has_committed:
                if os.getenv("PUSH_PACKAGE") == "true":
                    print("Pushing changes.")
                    git_push(this_repo_path, "HEAD:main", ci_skip=True)
                    git_push(aur_repo_path, "HEAD:master")
                else:
                    print("Not pushing because PUSH_PACKAGE isn't set to true.")
            else:
                print("Not pushing because nothing changed.")

    return True


if __name__ == "__main__":
    sys.exit(not bool(main(sys.argv[1:])))
